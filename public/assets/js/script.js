// /*
// mobilephone - properties(key): color, weight, model (value)
//             - method/functions: open, alarm, close, ring, send

// User (object)   > properties > username, email, password, gender
//                 > methods > login, logout
// */

// let grades = [85, 95, 100];

// /*
// Structure of an Object {} + key value pairs
// */

// let phone = {
//     color : 'red',
//     weight : '10 g',
//     model : 's7'
// };

// let grades = {
//     quiz1 : 85,
//     quiz2 : 95,
//     quiz3 : 100
// }

// //Object can contain another object

// let user = {
//     firstName : 'Juan',
//     lastName : 'Dela Cruz',
//     address : {
//         houseNum : 11,
//         street : 'Sapphire',
//         city : 'Tokyo',
//         country : 'Japan'
//     }
// }


// //Object can contain arrays too

// let user = {
//     firstName : 'Juan',
//     lastName : 'Dela Cruz',
//     address : {
//         houseNum : 11,
//         street : 'Sapphire',
//         city : 'Tokyo',
//         country : 'Japan'
//     }, 
//     email : ['juandelacruz@yahoo.com', 'jc@gmail.com'],
//     fullName : function() {
//         return this.firstName + ' ' + this.lastName; //this. > return properties
//     }
// }
// console.log(user);


//declare object
//1.
// let phone = Object();
//2.
// let mobilephone = {};


//adding properties
//1.
// let phone = Object();
//     phone.color = 'blue',
//     phone.weight = '115 g',
//     phone.model = 's7'
//     phone.ring = function() {
//         console.log('phone is ringing');
//     }

//     console.log(phone);


// //2.
// let mobilephone = {
//     color : 'red',
//     weight : '125 g',
//     model : 'iPhone10',
//     ring : function() {
//         console.log('mobile phone is ringing');
//     }
// };
// console.log(mobilephone);


// //accessing object properties
// //1. dot notation
// console.log(mobilephone.color);
// console.log(phone.model);

// //2. square bracket
// console.log(mobilephone['model']);


// //re-assign values
// mobilephone.color = 'black';
// mobilephone.ring = function() {
//     console.log('black is ringing');
// }
// console.log(mobilephone.color);
// mobilephone.ring();


// //delete
// delete mobilephone.color
// // console.log(mobilephone);

// delete mobilephone.ring;
// console.log(mobilephone);


//OBJECT CONSTRUCTOR - is a function that initialize an object

// //simple constructor
// let Pokemon = {
//     name : 'pikachu',
//     attack : 'lightning',
//     health : 100
// }
// console.log(Pokemon);

//object constructor - is a function that initialize an object
// //syntax for this. (name-attribute)
// function Pokemon(name) {
//     this.name = "pokemon name";
//     this.attack = "pokemon attack";
// }

function Pokemon(name) {
    this.name = name
    this.health = 100
    this.attack = function(target) {
        console.log(`${this.name} attacked ${target.name}`);
        target.health -= 10;
        console.log(`${target.name}'s health is now ${target.health}`)
        target.health += 20;
        console.log(`${this.name}'s health is now ${target.health}`)
    }
}
let Pikachu = new Pokemon("Pikachu"); //declare new value
let Geodude = new Pokemon("Geodude");
Pikachu.attack(Geodude);



/*
function Person(firstName, lastName) {
    this.name = `${firstName} ${lastName}`
    this.health = 100

    this.updateName = function(newName){
        this.name = newName;
    }
    this.attack = function(target) {
        console.log(`${this.name} attacked ${target.name}`);
        target.health -= 10;
        console.log(`${target.name}'s health is now ${target.health}`)
        this.health += 20;
        console.log(`${this.name}'s health is now ${this.health}`)
    }
}
let janine = new Person("Janine", "Bustamante");
janine.name = "kamote"
let julian = new Person("Julian", "Ponce");
janine.attack(julian);
*/





